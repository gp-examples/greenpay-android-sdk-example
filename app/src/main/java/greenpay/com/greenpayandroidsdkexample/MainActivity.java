package greenpay.com.greenpayandroidsdkexample;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;
// 2. Import GreenPay packages
import com.greenpay.sdk.GreenPay;
import com.greenpay.sdk.config.GreenPayConfig;
import com.greenpay.sdk.exceptions.ConfigurationException;
import com.greenpay.sdk.exceptions.GreenPayException;
import com.greenpay.sdk.models.GreenPayAddress;
import com.greenpay.sdk.models.GreenPayCheckoutOrderResult;
import com.greenpay.sdk.models.GreenPayCreditCard;
import com.greenpay.sdk.models.GreenPayOrderInfo;
import com.greenpay.sdk.models.GreenPayOrderResult;
import com.greenpay.sdk.models.GreenPayProduct;
import com.greenpay.sdk.models.GreenPayTokenizeCardCheckoutOrderResult;
import com.greenpay.sdk.models.GreenPayTokenizeCardOrderInfo;
import com.greenpay.sdk.models.GreenPayTokenizedCard;
import com.greenpay.sdk.utils.ResponseCallback;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Context AppContext;

    Button checkoutTokenizedCardButton;

    GreenPayOrderInfo gpOrderInfo = this.getOrderInfo();
    GreenPayCreditCard gpCreditCard = this.getCreditCard();
    GreenPayTokenizedCard gpTokenizedCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppContext = this;
        checkoutTokenizedCardButton = findViewById(R.id.buttonCheckoutTokenizeCard);

        // 3. Create a GreenPayConfig object to hold the SDK Configuration (Replace with your own values, these are invalid ones).
        GreenPayConfig greenPayConfig = new GreenPayConfig();
        greenPayConfig.setGreenPaySecret(""); //secret key provided by Greenpay
        greenPayConfig.setGreenPayMerchandId(""); //merchantId provided by Greenpay
        greenPayConfig.setGreenPayTerminal(""); //terminalId provided by Greenpay
        greenPayConfig.setGreenPayPublicKey(""); //public key provided by Greenpay

        // 4. Obtain a reference to the SDK and initialize it.
        GreenPay greenPay = GreenPay.getInstance();
        // Call the initGreenPaySDK method before making any action with the following values:
        //  - An Android Context (the current activity (this) or the context of the app (this.getApplicationContext() or the desired Android context to use.
        //  - The configuration object holding the user configuration.
        greenPay.initGreenPaySDK(this.getApplicationContext(), greenPayConfig, true);

        //4.2 You can enable the log output in the Android Console while testing ** Remember to delete this in production **
        greenPay.setEnableLogs(true);

        //for API >= 23 REQUEST LOCATION PERMISSIONS
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
            }
        }
    }

    public void checkoutOrder(android.view.View view) {

        final GreenPay greenPay = GreenPay.getInstance();
        try {
            //Create gpOrder, in this case order is create when globally
            greenPay.createNewGreenPayOrder(gpOrderInfo, new ResponseCallback<GreenPayOrderResult>() {
                @Override
                public void onSuccess(GreenPayOrderResult response) {
                    Log.d("GreenPayTestApp", "Create Order Response from Activity: " + response.getSecurityInfo().getSession() + ", " + response.getSecurityInfo().getToken());

                    try {
                        // 6. Create a GreenPayCreditCard object to hold the card information to charge.


                        // 7. Using the GreenPayOrderResult object (which holds the session id and transaction token).
                        // call checkoutExistingOrder to process the payment
                        greenPay.checkoutExistingOrder(gpCreditCard, response, new ResponseCallback<GreenPayCheckoutOrderResult>() {
                            @Override
                            public void onSuccess(GreenPayCheckoutOrderResult response) {
                                if(response.getStatus() == 200) {
                                    Log.d("GreenPayTestApp", "Checkout Order Response from Activity: " + response.getStatus() + ": " + response.getAuthorization());
                                    Toast.makeText(getApplicationContext(), "Orden cobrada. Status: " + response.getStatus() + ". Auth: " + response.getAuthorization(), Toast.LENGTH_LONG).show();
                                } else {
                                    Log.d("GreenPayTestApp", "Checkout Order Response from Activity: " + response.getStatus() + ": " + response.getError().getErrorMessage());
                                    Toast.makeText(getApplicationContext(), "Orden no cobrada. Error: " + response.getError().getCode() + " " + response.getError().getErrorMessage(), Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onError(String errorReason) {
                                Toast.makeText(getApplicationContext(), "Error making the payment: " + errorReason, Toast.LENGTH_LONG).show();
                            }
                        });
                    } catch (GreenPayException | ConfigurationException e) {
                        Toast.makeText(getApplicationContext(), "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(String errorReason) {
                    Toast.makeText(getApplicationContext(), "Error creating order" + errorReason, Toast.LENGTH_LONG).show();

                }
            });
        } catch (GreenPayException | ConfigurationException e) {
            Toast.makeText(getApplicationContext(), "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    public void tokenizeCard(android.view.View view){

        final GreenPay greenPay = GreenPay.getInstance();

        // 8. Create tokenization order object
        final GreenPayTokenizeCardOrderInfo gpTokenizeCardOrderInfo = new GreenPayTokenizeCardOrderInfo();
        gpTokenizeCardOrderInfo.generateRandomRequestId();

        try {
            // 9. Call create the tokenization order
            greenPay.createNewTokenizeCardGreenPayOrder(gpTokenizeCardOrderInfo, new ResponseCallback<GreenPayOrderResult>(){

                public void onSuccess(GreenPayOrderResult response){

                    Log.d("GreenPayTestApp", "Create Tokenize Card Order Response from Activity: " + response.getSecurityInfo().getSession() + ", " + response.getSecurityInfo().getToken());

                    // 10. Using the GreenPayOrderResult object (which holds the session id and transaction token).
                    // call checkoutExistingTokenizeCardOrder to process the payment
                    try {
                        greenPay.checkoutExistingTokenizeCardOrder(gpCreditCard, response, new ResponseCallback<GreenPayTokenizeCardCheckoutOrderResult>() {
                            @Override
                            public void onSuccess(GreenPayTokenizeCardCheckoutOrderResult response) {
                                if(response.getStatus() == 201) {
                                    Log.d("GreenPayTestApp", "Checkout Tokenize Card Order Response from Activity: " + response.getToken());
                                    Toast.makeText(getApplicationContext(), "Tarjeta Tokenizada. Token: " + response.getToken(), Toast.LENGTH_LONG).show();

                                    gpTokenizedCard = new GreenPayTokenizedCard();
                                    gpTokenizedCard.setToken(response.getToken());
                                    checkoutTokenizedCardButton.setEnabled(true);
                                } else {
                                    if(response.getError() != null) {
                                        Log.d("GreenPayTestApp", "Checkout Tokenize Card Order Response from Activity: " + response);
                                        Toast.makeText(getApplicationContext(), "Tarjeta no Tokenizada. Error: " + response.getError().getCode() + " " + response.getError().getErrorMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }

                            @Override
                            public void onError(String errorReason) {
                                Toast.makeText(getApplicationContext(), "Error tokenizing the card: " + errorReason, Toast.LENGTH_LONG).show();
                            }
                        });
                    } catch (GreenPayException | ConfigurationException e) {
                        Toast.makeText(getApplicationContext(), "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onError(String errorReason) {
                    Toast.makeText(getApplicationContext(), "Error creating tokenization order: " + errorReason, Toast.LENGTH_LONG).show();

                }
            });
        } catch (GreenPayException | ConfigurationException e) {
            Toast.makeText(getApplicationContext(), "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void checkoutTokenizeCard(android.view.View view){
        final GreenPay greenPay = GreenPay.getInstance();

        // 11. create the payment order
        try {

            greenPay.createNewGreenPayOrder(gpOrderInfo, new ResponseCallback<GreenPayOrderResult>(){

                public void onSuccess(GreenPayOrderResult response){

                    Log.d("GreenPayTestApp", "Tokenize Card Create Order Response from Activity: " + response.getSecurityInfo().getSession() + ", " + response.getSecurityInfo().getToken());

                    // 12. Using the GreenPayOrderResult object (which holds the session id and transaction token).
                    // call checkoutExistingOrder to process the payment.
                    // The gpTokenizedCard is an GreenPayTokenizedCard object which holds the token representing the Credit Card.
                    try {
                        greenPay.checkoutExistingOrder(gpTokenizedCard, response, new ResponseCallback<GreenPayCheckoutOrderResult>() {
                            @Override
                            public void onSuccess(GreenPayCheckoutOrderResult response) {
                                if(response.getStatus() == 200) {
                                    Log.d("GreenPayTestApp", "Checkout with Tokenized Card Response from Activity, Authorization: " + response.getAuthorization());
                                    Toast.makeText(AppContext, "Pago realizado de forma exitosa:\nAutorización: " + response.getAuthorization(), Toast.LENGTH_LONG).show();
                                } else {
                                    Log.d("GreenPayTestApp", "Checkout Order Response from Activity: " + response.getStatus() + ": " + response.getError().getErrorMessage());
                                    Toast.makeText(getApplicationContext(), "Orden no cobrada. Error: " + response.getError().getCode() + " " + response.getError().getErrorMessage(), Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onError(String errorReason) {
                                Log.d("GreenPayTestApp", "Checkout with Tokenized Card Error" + errorReason);

                                Toast.makeText(AppContext, "Payment failed " + errorReason, Toast.LENGTH_LONG).show();
                            }
                        });
                    } catch (GreenPayException | ConfigurationException e) {
                        Toast.makeText(getApplicationContext(), "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onError(String errorReason) {
                    Toast.makeText(AppContext, "Error creating order " + errorReason, Toast.LENGTH_LONG).show();
                }
            });
        } catch (GreenPayException | ConfigurationException e) {
            Toast.makeText(getApplicationContext(), "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public GreenPayOrderInfo getOrderInfo () {
        GreenPayProduct product = new GreenPayProduct();
        product.setDescription("Telefono celular");
        product.setPrice(340000.00);
        product.setQuantity(1);
        product.setSkuId("COD120");
        product.setType("Fisico");
        List<GreenPayProduct> products = new ArrayList<GreenPayProduct>();
        products.add(product);
        GreenPayAddress address = new GreenPayAddress("CR", "C087 Freses");
        return new GreenPayOrderInfo("John Doe", "jdoe@example.com", "114880766", address, address, 100.00, "CRC", "My order", "Android-or-123", products);
    }

    public GreenPayCreditCard getCreditCard() {
        GreenPayCreditCard cc = new GreenPayCreditCard();
        cc.setCardHolder("Jhon Doe");
//        cc.setCardNumber("4094394314664828");
        cc.setCardNumber("4111111111111111");
        cc.setCvc("123");
        cc.setExpirationMonth(9);
        cc.setExpirationYear(21);
        cc.setNickname("visa2449");
        return cc;
    }

}
