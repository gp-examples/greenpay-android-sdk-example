# **GreenPay Android SDK Example**

GreenPay is a payments platform that allows wesites, desktop and mobile apps to process payments.

In order to process payments you must create an account on [GreenPay](https://www.greenpay.me/).

***Note:
The steps are numbered as they are marked in the code files.***

## Installation

1. The Android SDK is published as a [jCenter](https://bintray.com/) depedency. To install, simply add this line to your dependencies in your gradle module configuration file (build.gradle):

```gradle
implementation 'com.greenpay.android:greenpay:1.0.11'
```

***Note:
The SDK uses a minimun of Target API of 19. If you add the dependency to an Target SDK lower than 19 you will get this error when syncing the build.gradle file:***

```gradle
Manifest merger failed : uses-sdk:minSdkVersion 17 cannot be smaller than version 19 declared in library [com.greenpay.android:greenpay:0.0.3] /.gradle/caches/transforms-1/files-1.1/greenpay-0.0.3.aar/45bc8ce4877150e32eb1ecb2d92b2425/AndroidManifest.xml as the library might be using APIs not available in 17
Suggestion: use a compatible library with a minSdk of at most 17,
or increase this project's minSdk version to at least 19,
or use tools:overrideLibrary="com.greenpay.sdk" to force usage (may lead to runtime failures)

```

Add permissions in manifest

```xml
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
```

For API >= 23, include the following code to request location permissions

```java
if (Build.VERSION.SDK_INT >= 23) {
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }
}
```

## Usage

2. Import the GreenPay SDK packages with the objects required. An example is:

``` Java
import com.greenpay.sdk.GreenPay;
import com.greenpay.sdk.config.GreenPayConfig;
import com.greenpay.sdk.exceptions.ConfigurationException;
import com.greenpay.sdk.exceptions.GreenPayException;
import com.greenpay.sdk.models.GreenPayAddress;
import com.greenpay.sdk.models.GreenPayCheckoutOrderResult;
import com.greenpay.sdk.models.GreenPayCreditCard;
import com.greenpay.sdk.models.GreenPayOrderInfo;
import com.greenpay.sdk.models.GreenPayOrderResult;
import com.greenpay.sdk.models.GreenPayProduct;
import com.greenpay.sdk.models.GreenPayTokenizeCardCheckoutOrderResult;
import com.greenpay.sdk.models.GreenPayTokenizeCardOrderInfo;
import com.greenpay.sdk.models.GreenPayTokenizedCard;
import com.greenpay.sdk.utils.ResponseCallback;
```

3. You need to create a GreenPay ```GreenPayConfig``` object to configure the SDK with your account information. This information will be provided by the GreenPay Team when you create your GreenPay Account.

``` Java
GreenPayConfig greenPayConfig = new GreenPayConfig();
greenPayConfig.setGreenPaySecret(""); //secret key provided by Greenpay
greenPayConfig.setGreenPayMerchandId(""); //merchantId provided by Greenpay
greenPayConfig.setGreenPayTerminal(""); //terminalId provided by Greenpay
greenPayConfig.setGreenPayPublicKey(""); //public key provided by Greenpay
```

***Note:
The Public Key should be imported like the example above.***

4. Obtain a reference for the GreenPay SDK and initialize the SDK with:
* An Android Context (the current activity (this) or the context of the app (this.getApplicationContext() or the desired Android context to use.
* The configuration object holding the user configuration.
* Boolean to enable sandbox mode

```java
GreenPay greenPay = GreenPay.getInstance();
greenPay.initGreenPaySDK(this.getApplicationContext(), greenPayConfig, true);
```
4.1 You can enable log output to the Android Logcat Console (By default the SDK doesn't log to the console the output).

```java
greenPay.setEnableLogs(true);
```

## Making a Payment

5. First, a order has to be created from your backend to GreenPay. This will generate a Session Id and a Transaction Token. You need these values in order to make a payment. You will need to create a ```GreenPayOrderResult``` object to hold these values.

6. Create a GreenPayCreditCard object to hold the card information to charge.

``` java
final GreenPayCreditCard gpCreditCard = new GreenPayCreditCard();
gpCreditCard.setCardHolder("Jhon Doe");
gpCreditCard.setCardNumber("4795736054664396");
gpCreditCard.setCvc("123");
gpCreditCard.setExpirationMonth(9);
gpCreditCard.setExpirationYear(21);
gpCreditCard.setNickname("visa2449");
```

7. From your app you can call ```checkoutExistingOrder``` with the Credit Card to process and the Session Id and Transaction Token obtained from your backend (the amount was sent when creating the order from your backend).

```java
try {
    greenPay.checkoutExistingOrder(gpCreditCard, response, new ResponseCallback<GreenPayCheckoutOrderResult>() {
        
        @Override
        public void onSuccess(GreenPayCheckoutOrderResult response) {
            Log.d("GreenPayTestApp", "Checkout Order Response from Activity: " + response.getStatus() + ": " + response.getAuthorization());
            Toast.makeText(getApplicationContext(), "Orden cobrada. Status: " + response.getStatus() + ". Auth: " + response.getAuthorization(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(String errorReason) {
            Toast.makeText(getApplicationContext(), "Error al Cobrar la Tarjeta: " + errorReason, Toast.LENGTH_LONG).show();
        }
            
    });
} catch (GreenPayException e) {
    e.printStackTrace();
} catch (ConfigurationException e) {
    e.printStackTrace();
}
```
The returning object  ```GreenPayCheckoutOrderResult``` will have the transaction confirmation from GreenPay.

***Note:
For testing purposes, in this example the Order is created from the app, but in production it should be created in your backend system and then the Session Id and a Transaction Token should be sent to your app to continue the Checkout process like above.***

## Tokenizing a Card 

8. First, a order has to be created from your backend to GreenPay. This will generate a Session Id and a Transaction Token. You need these values in order to tokenize a card. You will need to create a ```GreenPayOrderResult``` object to hold these values.

9. Create a ```GreenPayCreditCard``` to hold the Credit Card information to tokenize.

``` java
final GreenPayCreditCard gpCreditCard = new GreenPayCreditCard();
gpCreditCard.setCardHolder("Jhon Doe");
gpCreditCard.setCardNumber("4795736054664396");
gpCreditCard.setCvc("123");
gpCreditCard.setExpirationMonth(9);
gpCreditCard.setExpirationYear(21);
gpCreditCard.setNickname("visa2449");
```
10. Second, from your app you can call ```checkoutExistingTokenizeCardOrder```  with the Credit Card to tokenize and the Session Id and Transaction Token obtained from your backend (the amount was sent when creating the order from your backend).

```java
try {
    GreenPayTokenizedCard gpTokenizedCard;
    
    greenPay.checkoutExistingTokenizeCardOrder(gpCreditCard, response, new ResponseCallback<GreenPayTokenizeCardCheckoutOrderResult>() {

        @Override
        public void onSuccess(GreenPayTokenizeCardCheckoutOrderResult response) {
            Log.d("GreenPayTestApp", "Checkout Tokenize Card Order Response from Activity: " + response.getToken());
            Toast.makeText(getApplicationContext(), "Tarjeta Tokenizada. Token: " + response.getToken(), Toast.LENGTH_LONG).show();

            gpTokenizedCard = new GreenPayTokenizedCard();
            gpTokenizedCard.setToken(response.getToken());
            
        }

        @Override
        public void onError(String errorReason) {
            Toast.makeText(getApplicationContext(), "Error al Tokenizar Tarjeta: " + errorReason, Toast.LENGTH_LONG).show();
        }
    });
} catch (GreenPayException e) {
    e.printStackTrace();
} catch (ConfigurationException e) {
    e.printStackTrace();
}
```
The returning object  ```GreenPayTokenizeCardCheckoutOrderResult``` will have the Card Token representing the Card in GreenPay.

## Making a Payment with a Tokenized Card

11. First, a order has to be created from your backend to GreenPay. This will generate a Session Id and a Transaction Token. You need these values in order to make a payment. You will need to create a ```GreenPayOrderResult``` object to hold these values.

12. Second, from your app you can call ```checkoutExistingOrder``` with the Tokenized Card to process and the Session Id and Transaction Token obtained from your backend (the amount was sent when creating the order from your backend). This Tokenized Card is an ```GreenPayTokenizedCard``` with the token representing the Credit Card.

```java
try {
    //gpTokenizedCard is an already tokenized card
    //order is an already created order
    greenPay.checkoutExistingOrder(gpTokenizedCard, order, new ResponseCallback<GreenPayCheckoutOrderResult>() {
    
        @Override
        public void onSuccess(GreenPayCheckoutOrderResult response) {
            Log.d("GreenPayTestApp", "Checkout with Tokenized Card Response from Activity, Authorization: " + response.getAuthorization());
            Toast.makeText(AppContext, "Pago realizado de forma exitosa:\nAutorización: " + response.getAuthorization(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(String errorReason) {
            Toast.makeText(getApplicationContext(), "Error al pagar orden con token: " + errorReason, Toast.LENGTH_LONG).show();
        }
    });
} catch (GreenPayException e) {
    e.printStackTrace();
} catch (ConfigurationException e) {
    e.printStackTrace();
}
```
The returning object  ```GreenPayCheckoutOrderResult``` will have the transaction confirmation from GreenPay.
